#
# spec file for package shmux
#
# Copyright (c) 2021 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#


Name:           shmux
Version:        1.0.3
Release:        0
Summary:	Shell Multiplexor
License:        BSD-3-Clause
URL:            https://github.com/shmux/shmux
Source:         https://github.com/shmux/shmux/archive/refs/tags/v%{version}.tar.gz
BuildRequires:  ncurses-devel

%description
shmux is program for executing the same command on many hosts in parallel.

%prep
%setup -q

%build
%configure
%if 0%{?rhel_version}
make %{?_smp_mflags}
%else
%make_build
%endif

%install
%make_install

%files
%license LICENSE
%doc README.md CHANGES
%{_bindir}/%{name}
%if 0%{?fedora}
%{_mandir}/man1/%{name}.*
%else
%{_mandir}/man1/%{name}.1%{?ext_man}
%endif
%dir %{_datadir}/%{name}
%attr (755,-,-) %{_datadir}/%{name}/mcmd.sh

%changelog

